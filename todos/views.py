from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todolist_object": todo_list,
    }
    return render(request, "todos/todo_list.html", context)

def todo_list_detail (request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "detail_list": todo_detail,
    }
    return render(request, "todos/todo_detail.html", context)

def todo_list_create (request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    edit_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = edit_list)
        if form.is_valid():
            list = form.save()
            return redirect ("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance = edit_list)
    context = {
        "list_edit": edit_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            items = form.save()
            return redirect("todo_list_detail", id=items.list.id )
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/items_create.html", context)

def todo_item_update(request, id):
    update_list = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=update_list)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=update_list)
    context = {
        "list_update": update_list,
        "form": form,
    }
    return render(request, "todos/update.html", context)


# def create_recipe(request):
#     if request.method == "POST":
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save(False)
#             recipe.author = request.user
#             recipe.save()
#             return redirect("recipe_list")
#     else:
#         form = RecipeForm()

#     context = {
#         "form": form
#     }
#     # Put the form in the context
#     return render(request, "recipes/create.html", context)
#     # Render the HTML template with the form






# def show_recipe(request, id):
#     recipe = get_object_or_404(Recipe, id=id)
#     context = {
#         "recipe_object": recipe,
#     }
#     return render(request, "recipes/detail.html", context)
